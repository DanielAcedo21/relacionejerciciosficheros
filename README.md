# Ejercicio de Ficheros (Tema 2) para la asignatura Acceso a Datos #
### Por Daniel Acedo Calderón ###

# Descripción de los ejercicios #

## Ejercicio 1 ##
Este ejercicio se trata de una lista de contactos o agenda. Contamos con una lista en la que podemos ver el nombre, el teléfono y el email (si tienen) de los contactos que agreguemos. Para añadir un contacto se pulsa el botón de añadir que nos llevará a una nueva activity donde podremos rellenar los datos. El nombre y el teléfono son obligatorios. Se controla que el teléfono solo contenga números o espacios. En el caso de que introduzcamos un email debe de tener el formato adecuado.

![EJ1_Anadir.JPG](https://bitbucket.org/repo/9Eog4X/images/3914269505-EJ1_Anadir.JPG)

Los contactos se pueden borrar pulsando el botón rojo a su lado en la lista.

![EJ1_Principal.JPG](https://bitbucket.org/repo/9Eog4X/images/4158972649-EJ1_Principal.JPG)

Los contactos se guardan en formato JSON para conservarlos de forma persistente.

![EJ1_Fichero.JPG](https://bitbucket.org/repo/9Eog4X/images/2094458681-EJ1_Fichero.JPG)


## Ejercicio 2 ##

En este ejercicio tenemos una lista de alarmas (máximo 5) que contienen los minutos que se ejecutan y el mensaje que se muestra cuando se acaba cada una. Las alarmas se guardan en un fichero interno para mantenerlas cuando se inicia la aplicación de nuevo.

![EJ2_Principal.JPG](https://bitbucket.org/repo/9Eog4X/images/525589866-EJ2_Principal.JPG)

Las alarmas se ejecutan en orden, cuando se termina una la siguiente empieza a ejecutarse. Al terminar cada alarma se muestra el mensaje que contiene la alarma y suena un sonido. Cuando se acaban todas las alarmas de la lista se muestra una notificación en la barra de notificaciones avisando de que no quedan más alarmas.

![EJ2_Notificacion.JPG](https://bitbucket.org/repo/9Eog4X/images/1422552131-EJ2_Notificacion.JPG)

Se pueden borrar y añadir alarmas, además hay una opción para que se sigan contando las alarmas al salir de la aplicación, aunque en ese caso no se pueden parar.

![EJ2_Anadir.JPG](https://bitbucket.org/repo/9Eog4X/images/3822974785-EJ2_Anadir.JPG)

## Ejercicio 3 ##

Con esta aplicación se pueden calcular los días fértiles de una mujer a partir de la fecha de inicio de su última menstruación y la duración de su ciclo. Estos datos se introducen por pantalla y al pulsar el botón de calcular se nos mostrará una ventana con los resultados conteniendo el día donde comienza la ovulación, los días entre los que es periodo fértil y además nos avisa si nos encontramos en uno de esos días o no. Además después de cada consulta se guardará en un fichero externo el primer y último día del periodo fértil.

![EJ3_Principal.JPG](https://bitbucket.org/repo/9Eog4X/images/1348246912-EJ3_Principal.JPG)

![EJ3_Resultado.JPG](https://bitbucket.org/repo/9Eog4X/images/2118402381-EJ3_Resultado.JPG)

## Ejercicio 4 ##

En este ejercicio podemos abrir paginas webs eligiendo 3 métodos diferentes: java.net, Android Asynchronous Http Client  y Volley. Podemos introducir la url del sitio donde queramos hacer una petición y si se produce con éxito se mostrara el resultado en el elemento WebView. En caso de error se notificará.

Una vez cargada una página podemos guardar su contenido en un fichero con el nombre que elijamos, pero si aún no se ha cargado nada dará error.

![EJ4_Principal.JPG](https://bitbucket.org/repo/9Eog4X/images/2768001545-EJ4_Principal.JPG)

## Ejercicio 5 ##

Aquí podremos cargar una galería de imagen descargada de internet. Tendremos que introducir una url que nos lleve a un fichero de texto que contenga una url a una imagen en cada línea, si esto es así se creará una lista de imágenes que podremos recorrer con los botones de arriba y abajo. Se mostrará en todo momento en que posición de la galería nos encontramos y cuantas imágenes tiene. La carga de las imágenes se hace usando la galería Picasso. En el caso no poder cargar algunas de las imágenes se mostrara una imagen de error en su lugar, y mientras se carga una foto aparecerá una animación.

![EJ5_Principal.JPG](https://bitbucket.org/repo/9Eog4X/images/306308518-EJ5_Principal.JPG)

![EJ5_Error.JPG](https://bitbucket.org/repo/9Eog4X/images/2805864985-EJ5_Error.JPG)

## Ejercicio 6 ##

El ejercicio 6 está sacado de la relación anterior en el que hice lo mismo, pero le he hecho un par de mejoras.

En primer lugar he sustituido la clase AsyncTask con HttpUrlConnection y he usado Asynchronous Http Client con la RestApi en su lugar utilizando el JsonHttpResponseHandler.
El siguiente cambio es que he arreglado el fallo que hacia que cuando actualizabas la información con el servidor se reseteaba a seleccionar la primera divisa de la lista, ahora recuerda la anterior que tenias elegida antes de actualizar. También ahora cuando cambias de divisa se hace una conversión automática usando la nueva divisa elegida.

![EJ6_Principal.JPG](https://bitbucket.org/repo/9Eog4X/images/305224471-EJ6_Principal.JPG)

## Ejercicio 7 ##

Por último, este ejercicio permite subir ficheros a un servidor que cuente con un programa PHP.

Con el botón de la izquierda se abrirá un gestor de archivos instalado en el móvil para seleccionar un archivo que queremos subir. Una vez abierto pulsamos el botón de subir y se realizará una llamada al servidor para subir el archivo.

![EJ7_Principal.JPG](https://bitbucket.org/repo/9Eog4X/images/737922980-EJ7_Principal.JPG)

El fichero se guarda dos niveles por encima de donde está el php para impedir que sea accesible directamente desde el navegador al estar fuera de la carpeta del servidor apache. Solo se aceptan ficheros de hasta 1MB y de formato jpg, png, html, txt, mp3 o pdf. Si el archivo ya existe se rechazará.

También se utiliza una comprobación de contraseña para que solo se acepten llamadas de esta aplicación.
En el caso de que no coincidiesen los códigos mandados por POST se rechazaría la subida.

![EJ7_permiso.JPG](https://bitbucket.org/repo/9Eog4X/images/4192273922-EJ7_permiso.JPG)

Adjunto con la subida el archivo php por si surge algún error.